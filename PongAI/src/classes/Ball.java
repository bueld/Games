package classes;

import java.util.ArrayList;

import javafx.geometry.Point2D;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeType;

public class Ball extends Ellipse
{
	private double bxd, byd;
	private boolean moving = false;
	private double ballSpeed;
	private int restartCooldown;

	private final double degMax = 45;

	private double pevNumber = 8;
	private int iteration;

	private Ball nextBall;

	public Ball(double radius, double speed, int i, double pevNumber)
	{
		iteration = i;
		this.setRadiusX(radius);
		this.setRadiusY(radius);
		this.setStrokeWidth(1);
		this.setStrokeType(StrokeType.INSIDE);
		this.setTranslateX(300 - radius / 2);
		this.setTranslateY(250 - radius / 2);
		this.pevNumber = pevNumber;
		ballSpeed = speed;
		if (iteration < pevNumber)
		{
			if (iteration == 0)
			{
				nextBall = new Ball(radius / 2, speed, iteration + 1,pevNumber);
			} else
			{
				nextBall = new Ball(radius, speed, iteration + 1,pevNumber);
			}
			nextBall.setFill(Color.WHITESMOKE);
			nextBall.setOpacity(1 - ((iteration + 1) / pevNumber));
			nextBall.setStroke(Color.TRANSPARENT);
		}
		
		
	}

	public int moveBall(Rectangle top, Rectangle bot, Rectangle p1,
			Rectangle p2, Rectangle bounds)
	{

		int a = 1;

		if (iteration != 0)
		{
			a = (int) (60 / ballSpeed);
		}

		for (int i = 0; i < a; i++)
		{

			if (moving)
			{
				if (this.getTranslateY() + byd - 8 < top.getTranslateY()
						+ top.getHeight())
				{
					this.setTranslateY(
							2 * (top.getTranslateY() + top.getHeight() + 8)
									+ Math.abs(byd) - getTranslateY());
					byd = Math.abs(byd);
				} else if (this.getTranslateY() + byd + 8 > bot.getTranslateY())
				{
					this.setTranslateY(2 * (bot.getTranslateY() - 8)
							- getTranslateY() - Math.abs(byd));
					byd = -Math.abs(byd);
				} else
				{
					this.setTranslateY(this.getTranslateY() + byd);
				}
				if (this.getTranslateX() + bxd - 8 < p1.getTranslateX()
						+ p1.getWidth()
						&& this.getTranslateY() + byd + 8 > p1.getTranslateY()
						&& this.getTranslateY() + byd - 8 < p1.getTranslateY()
								+ p1.getHeight())
				{
					bxd = Math.abs(bxd);
					reflecFromPaddle(0, p1);
					this.setTranslateY(this.getTranslateY() + byd);
				} else if (this.getTranslateX() + bxd + 8 > p2.getTranslateX()
						&& this.getTranslateY() + byd + 8 > p2.getTranslateY()
						&& this.getTranslateY() + byd - 8 < p2.getTranslateY()
								+ p2.getHeight())
				{
					bxd = -Math.abs(bxd);
					reflecFromPaddle(1, p2);
					this.setTranslateY(this.getTranslateY() + byd);
				}
				this.setTranslateX(this.getTranslateX() + bxd);
			}
		}
		if (restartCooldown >= 0)
		{
			restartCooldown--;
		}
		if (restartCooldown == 0)
		{
			moving = true;
		}

		if (nextBall != null)
		{
			nextBall.setTranslateX(getTranslateX());
			nextBall.setTranslateY(getTranslateY());
			nextBall.bxd = bxd;
			nextBall.byd = byd;
			nextBall.moveBall(top, bot, p1, p2, bounds);
		}

		return checkGoal(bounds);
	}

	private void reflecFromPaddle(int id, Rectangle p)
	{
		double midOffset = (this.getTranslateY() - p.getTranslateY()
				- p.getHeight() / 2) / p.getHeight() * 2;
		if (midOffset < -1)
		{
			midOffset = -1;
		} else if (midOffset > 1)
		{
			midOffset = 1;
		}

		double alpha = midOffset * degMax;

		double deg = Math.toDegrees(Math.asin(-byd / ballSpeed));

		double deg2 = 2 * alpha - deg;

		if (deg2 < 90 && deg2 > 85)
		{
			deg2 = 85;
		} else if (deg2 >= 90 && deg2 < 95)
		{
			deg2 = 95;
		} else if (deg2 > -95 && deg2 < -90)
		{
			deg2 = -95;
		} else if (deg2 < -85 && deg2 >= -90)
		{
			deg2 = -85;
		}

		bxd = Math.cos(Math.toRadians(deg2)) * ballSpeed * Math.pow(-1, id);
		byd = Math.sin(Math.toRadians(deg2)) * ballSpeed;
	}

	private int checkGoal(Rectangle bounds)
	{
		if (iteration == 0)
		{
			if (this.getTranslateX() < bounds.getTranslateX())
			{
				reset(false, bounds);
				return -1;
			}
			if (this.getTranslateX() > bounds.getWidth())
			{
				reset(false, bounds);
				return 1;
			}
		}

		setOpacity(1 - (iteration / pevNumber));

		if (this.getTranslateX() < 0)
		{
			setTranslateX(bounds.getTranslateX());
			setOpacity(0);
		}
		if (this.getTranslateX() > bounds.getWidth())
		{
			setTranslateX(bounds.getTranslateX() + bounds.getWidth());
			setOpacity(0);
		}

		return 0;
	}

	public void reset(boolean move, Rectangle bounds)
	{
		this.setTranslateX(bounds.getWidth() / 2);
		this.setTranslateY(bounds.getHeight() / 2);

		double start = Math.random() * 360;

		while ((start > 60 && start < 120) || (start > 240 && start < 300))
		{
			start = Math.random() * 360;
		}

		bxd = Math.cos(Math.toRadians(start)) * ballSpeed;
		byd = Math.sin(Math.toRadians(start)) * ballSpeed;

		moving = move;
		restartCooldown = 100;
		if (nextBall != null)
		{
			nextBall.reset(move, bounds);
		}
	}
	
	public void togglePreview(boolean prev)
	{
		if(prev && nextBall == null)
		{
			nextBall = new Ball(this.getRadiusX()/2,ballSpeed,1,pevNumber);
			
			nextBall.setFill(Color.WHITESMOKE);
			nextBall.setOpacity(1 - ((iteration + 1) / pevNumber));
			nextBall.setStroke(Color.TRANSPARENT);
		}
		if(!prev)
		{
			nextBall = null;
		}
	}

	public void setMoving(boolean move)
	{
		moving = move;
		if (nextBall != null)
		{
			nextBall.setMoving(move);
		}
	}

	public Point2D getPos()
	{
		return new Point2D(getTranslateX(), getTranslateY());
	}

	public Point2D getVel()
	{
		return new Point2D(bxd, byd);
	}

	public ArrayList<Ball> getNext(ArrayList<Ball> a)
	{
		if (nextBall != null)
		{
			a.add(nextBall);
			return nextBall.getNext(a);
		}
		return a;
	}

	public void setSpeed(double s)
	{
		bxd = bxd / ballSpeed * s;
		byd = byd / ballSpeed * s;
		ballSpeed = s;
		if (nextBall != null)
		{
			nextBall.setSpeed(s);
		}
	}
	
//	!!!!
//	!! Only invoke by Classes 'App' << adjustPrevNumber() >> !!
//	!!
//	!! If invoked by other means, Program is sure to crash
//	!!
	
	public void setPevNumber(int newNumber)
	{
		pevNumber = newNumber;
	}
	
//	!!!!
	
	public Ball getOnlyNext()
	{
		return nextBall;
	}

	public int getCooldown()
	{
		return restartCooldown;
	}
}