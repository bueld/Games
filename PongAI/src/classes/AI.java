package classes;

import java.util.ArrayList;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.geometry.Point2D;
import javafx.util.Duration;

public class AI
{
	private Timeline timer;

	private double setPointY;

	private Point2D paddleCoords, enemyCoords;
	private ArrayList<Point2D> ballCoords, ballVel;
	private ArrayList<Integer> cooldowns;

	private String key1, key2, key3, key4;

	private int method = 0;
	private double topBound, botBound, leftBound, rightBound, pHeight;
	private double degMax = 45;

	private double offset = 0;
	private int bounces = 0;

	private boolean running = false;

	private int index = 0;

	private double aim = 0;

	private ArrayList<String> pressedKeys = new ArrayList<String>();

	public AI(String key1, String key2, String key3, String key4, double bot,
			double top, double left, double right, double pH, int method)
	{
		this.paddleCoords = new Point2D(0, 0);
		this.enemyCoords = new Point2D(0, 0);
		ballCoords = new ArrayList<>();
		ballVel = new ArrayList<>();
		cooldowns = new ArrayList<>();
		leftBound = left;
		rightBound = right;
		pHeight = pH;
		this.key1 = key1;
		this.key2 = key2;
		this.key3 = key3;
		this.key4 = key4;
		this.topBound = top;
		this.botBound = bot;
		this.method = method;

		createTimeLine();

	}

	public void updateValues(Point2D paddleCoords, Point2D enemyCoords,
			ArrayList<Ball> balltz)
	{
		this.paddleCoords = paddleCoords;
		this.enemyCoords = enemyCoords;
		ballCoords.clear();
		ballVel.clear();
		cooldowns.clear();
		for (Ball b : balltz)
		{
			ballCoords.add(b.getPos());
			ballVel.add(b.getVel());
			cooldowns.add(b.getCooldown());
		}
	}

	private void createTimeLine()
	{
		timer = new Timeline();
		timer.setCycleCount(Timeline.INDEFINITE);
		timer.getKeyFrames().add(new KeyFrame(Duration.millis(3), e ->
		{
			refreshMovement();
		}));
	}

	private void refreshMovement()
	{
		// offset = (Math.random() - 0.5) * 40;

		index = 0;
		double balld = 0;
		double balltime = 0;
		switch (method)
		{
			case 0 :
				balld = paddleCoords.distance(ballCoords.get(0));
				break;

			case 2 :
				balld = paddleCoords.distance(ballCoords.get(0));
				break;

			case 3 :
				balld = paddleCoords
						.distance(new Point2D(ballCoords.get(0).getX(),
								ballCoords.get(0).getY() / 5));
				break;

			case 4 :
				balltime =(paddleCoords.getX() - ballCoords.get(0).getX())
						/ ballVel.get(0).getX()
						+ cooldowns.get(0);
				break;

			case 6 :
				balltime = Math
						.abs(paddleCoords.getX() - ballCoords.get(0).getX())
						/ Math.abs(ballVel.get(0).getX()) + cooldowns.get(0);
				break;

			case 7 :
				balltime = Math
						.abs(paddleCoords.getX() - ballCoords.get(0).getX())
						/ Math.abs(ballVel.get(0).getX()) + cooldowns.get(0);
				break;
		}

		for (int i = 1; i < ballCoords.size(); i++)
		{
			double balldnew = 0;
			double balltimenew = 0;
			switch (method)
			{
				case 0 :
					balldnew = paddleCoords.distance(ballCoords.get(i));
					break;
				case 2 :
					balldnew = paddleCoords.distance(ballCoords.get(i));
					break;

				case 3 :
					balldnew = paddleCoords
							.distance(new Point2D(ballCoords.get(i).getX(),
									ballCoords.get(i).getY() / 5));
					break;

				case 4 :
					balltimenew = (paddleCoords.getX() - ballCoords.get(i).getX())
							/ ballVel.get(i).getX()
							+ cooldowns.get(i);
					break;

				case 6 :
					balltimenew = Math
							.abs(paddleCoords.getX() - ballCoords.get(i).getX())
							/ Math.abs(ballVel.get(i).getX())
							+ cooldowns.get(i);
					break;

				case 7 :
					balltimenew = Math
							.abs(paddleCoords.getX() - ballCoords.get(i).getX())
							/ Math.abs(ballVel.get(i).getX())
							+ cooldowns.get(i);
					break;
			}
			
//			System.out.println(balltime + "   " + balltimenew);
			
			if (balldnew < balld && method < 4)
			{
				index = i;
				balld = balldnew;
			} else if ((balltimenew < balltime || balltime < 0) && method >= 4 && balltimenew >= 0)
			{
				index = i;
//				System.out.println(paddleCoords.getX());
				balltime = balltimenew;
			}
		}

		balld = paddleCoords.distance(ballCoords.get(index));

		if (method == 0)
		{
			setPointY = ballCoords.get(index).getY();
		} else
		{
			predictBallEndPoint();
		}
		if (method >= 6)
		{
			aimAt(index, aim);
		}
		if (method == 7)
		{
			chooseAim();
		}

		if (paddleCoords.getY() > setPointY + 8 + offset)
		{
			removeAllKeys();
			addKey(key1);
		} else if (paddleCoords.getY() < setPointY - 8 + offset)
		{
			removeAllKeys();
			addKey(key2);
		} else if (paddleCoords.getY() > setPointY + 1 + offset)
		{
			removeAllKeys();
			addKey(key3);
		} else if (paddleCoords.getY() < setPointY - 1 + offset)
		{
			removeAllKeys();
			addKey(key4);
		} else
		{
			removeAllKeys();
		}
	}

	private void predictBallEndPoint()
	{
		setPointY = ballCoords.get(index).getY() + ballVel.get(index).getY()
				* (paddleCoords.getX() - ballCoords.get(index).getX())
				/ ballVel.get(index).getX();
		bounces = 0;
		while (setPointY < topBound || setPointY > botBound)
		{
			if (setPointY > botBound)
			{
				setPointY = 2 * botBound - setPointY;
			} else
			{
				setPointY = 2 * topBound - setPointY;
			}
			bounces++;
		}
	}

	private void aimAt(int i, double a)
	{
		if (a < 0)
		{
			a = 0;
		} else if (a > botBound - topBound)
		{
			a = botBound - topBound;
		}
		aim = a;
		a += topBound;

		boolean repeat = false;
		int iteration = 0;

		do
		{
			double spd = ballVel.get(i).distance(0, 0);
			double dy = -a + setPointY;
			double w = rightBound - leftBound;
			double deg = Math.toDegrees(Math
					.asin(ballVel.get(i).getY() * Math.pow(-1, bounces) / spd));
			double deg2 = Math.toDegrees(Math.atan(dy / w));
			double midOffset = (deg2 + deg) / (2 * degMax);
			if (midOffset < -1)
			{
				midOffset = -1;
			} else if (midOffset > 1)
			{
				midOffset = 1;
			}
			offset = midOffset * pHeight / 2;

			repeat = false;

			if (setPointY + offset > botBound + 8 - pHeight / 2)
			{
				a = 2 * botBound - a;
				repeat = true;
			} else if (setPointY + offset < topBound - 8 + pHeight / 2)
			{
				a = 2 * topBound - a;
				repeat = true;
			}
			iteration++;
		} while (repeat && iteration < 10);
	}

	private void chooseAim()
	{
		if (enemyCoords.getY() > (topBound + botBound) / 2)
		{
			setAim(20);
		} else
		{
			setAim(botBound - topBound - 20);
		}
	}

	private void addKey(String key)
	{
		if (!pressedKeys.contains(key))
		{
			pressedKeys.add(key);
		}
	}

	private void removeKey(String key)
	{
		if (pressedKeys.contains(key))
		{
			pressedKeys.remove(key);
		}
	}

	private void removeAllKeys()
	{
		pressedKeys.clear();
	}

	public ArrayList<String> getPressedKeys()
	{
		return pressedKeys;
	}

	public void startTimer()
	{
		timer.play();
		running = true;
	}

	public void stopTimer()
	{
		timer.pause();
		running = false;
		pressedKeys.clear();
	}

	public void setMethod(int m)
	{
		method = m;
		if (method != 6)
		{
			offset = 0;
		}
	}

	public boolean isRunning()
	{
		return running;
	}

	public int getMethod()
	{
		return method;
	}

	public double getAim()
	{
		return aim;
	}

	public void setAim(double a)
	{
		if (a < 0)
		{
			a = 0;
		} else if (a > botBound - topBound)
		{
			a = botBound - topBound;
		}
		aim = a;
	}
}