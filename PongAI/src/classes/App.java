package classes;

import java.util.ArrayList;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.geometry.VPos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.Separator;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.effect.Light;
import javafx.scene.effect.Lighting;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Line;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.StrokeLineJoin;
import javafx.scene.shape.StrokeType;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;
import javafx.util.Duration;

public class App extends Application
{

	private Scene scn;
	private StackPane stck;
	private Group playground;
	private Group options;

	private Rectangle bounds;
	private Line line;
	private Rectangle top, bot;
	private Rectangle statsBg;

	private Rectangle frame;

	private Paddle p1;
	private Paddle p2;

	private ArrayList<Ball> ballsOfSteel;
	private Group ballz;

	private ArrayList<String> keyPressing;

	private GridPane stats;
	private Label scoreA, scoreB;
	private String playerA = "M�la";
	private String playerB = "Baxi";

	private Button optionsButton;
	private Rectangle pausingBg;
	private Label pausing;

	// Options
	private Rectangle darken;
	private Rectangle optionsBg;
	private GridPane optionsGrid;
	private Label optionsTitle;
	private TextField editPlayerA, editPlayerB;
	private Label remainingCA, remainingCB;
	private Button setPlayerA, setPlayerB;
	private Button exitOptionsButton;
	private TextField editBallCount;
	private Label showBallNumber;
	private Button setBallCount;
	private TextField editBallSpeed;
	private Label showBallSpeed;
	private Button setBallSpeed;
	private ChoiceBox cAI1, cAI2;
	private Button tAI1, tAI2;
	private Button tPrev;
	private Slider pevNumber;

	private Timeline time;

	private int ballNumber = 2;
	private double speeeeeed = 3 * Math.PI;
	private double dsped = 4;
	private double sped1 = 0;
	private double sped2 = 0;
	private double zoom = 2;
	private int winsA, winsB;
	private final int borderWidth = 12;
	private final int padding = 8;
	private final int paddleHeight = 100;
	private final int nameLength = 20;
	private final int numberLength = 7;
	private final int labelLength = 37;
	private final String dots = "............................1";

	private double ballSpeed = 0.2;

	private boolean optionsMode = false;
	private boolean paused = false;

	private AI ai1;
	private AI ai2;
	private Ellipse a1, a2;

	public void init()
	{
		createPlayground();

		createKeyHandling();

		createAI();
	}

	private void createPlayground()
	{
		playground = new Group();
		options = new Group();

		Color border = Color.rgb(150, 40, 190);

		bounds = new Rectangle();
		bounds.setWidth(600);
		bounds.setHeight(500);
		bounds.setFill(Color.rgb(10, 3, 15));

		top = new Rectangle();
		top.setWidth(bounds.getWidth());
		top.setHeight(borderWidth);
		top.setFill(border);
		top.setStroke(border);

		bot = new Rectangle();
		bot.setWidth(bounds.getWidth());
		bot.setHeight(borderWidth);
		bot.setFill(border);
		bot.setTranslateY(bounds.getHeight() - bot.getHeight());
		bot.setStroke(border);

		line = new Line(top.getTranslateX() + top.getWidth() / 2,
				top.getTranslateY() + top.getHeight(),
				bot.getTranslateX() + bot.getWidth() / 2, bot.getTranslateY());
		line.setStroke(Color.rgb(255, 255, 255, 0.5));
		line.setFill(Color.TRANSPARENT);
		line.getStrokeDashArray().add(10d);
		line.setStrokeDashOffset(7);
		line.setStrokeWidth(2);

		createStats();

		ballsOfSteel = new ArrayList<Ball>();
		for (int i = 0; i < ballNumber; i++)
		{
			Color c = Color.hsb(Math.random() * 360, 1, 1);
			ballsOfSteel.add(new Ball(8, ballSpeed, 0, 8));
			ballsOfSteel.get(i).reset(false, bounds);
			ballsOfSteel.get(i).setFill(c);
			ballsOfSteel.get(i).setStroke(c.darker());
		}

		frame = new Rectangle();
		frame.setHeight(bounds.getHeight() - 2);
		frame.setWidth(bounds.getWidth() - 2);
		frame.setTranslateX(1);
		frame.setTranslateY(1);
		frame.setFill(Color.TRANSPARENT);
		frame.setStroke(border);
		frame.setStrokeWidth(padding + 1);
		frame.setStrokeType(StrokeType.OUTSIDE);

		p1 = new Paddle(true, bounds, paddleHeight);
		p2 = new Paddle(false, bounds, paddleHeight);

		createPausing();
		createOptions();

		ballz = new Group();
		for (Ball bl : ballsOfSteel)
		{
			ballz.getChildren().addAll(bl.getNext(new ArrayList<Ball>()));
		}
		ballz.getChildren().addAll(ballsOfSteel);

		playground.getChildren().addAll(bounds, line, top, bot, statsBg, stats);
		playground.getChildren().add(ballz);
		playground.getChildren().addAll(frame, p1, p2, pausingBg, pausing,
				options);
	}

	private void adjustBalls(int b)
	{
		// ballz.getChildren().removeAll(ballsOfSteel);
		ballz.getChildren().clear();
		ballsOfSteel.clear();
		ballNumber = b;
		for (int i = 0; i < ballNumber; i++)
		{
			Color c = Color.hsb(Math.random() * 360, 1, 1);
			ballsOfSteel.add(new Ball(8, ballSpeed, 0, 8));
			ballsOfSteel.get(i).reset(false, bounds);
			ballsOfSteel.get(i).setFill(c);
			ballsOfSteel.get(i).setStroke(c.darker());
		}

		for (Ball bl : ballsOfSteel)
		{
			ballz.getChildren().addAll(bl.getNext(new ArrayList<Ball>()));
		}
		ballz.getChildren().addAll(ballsOfSteel);
		updateAIAction();
	}

	private void togglePreview(boolean prev)
	{
		if (!prev && ballsOfSteel.get(0).getOnlyNext() != null)
		{
			for (Ball b : ballsOfSteel)
			{
				ballz.getChildren().removeAll(b.getNext(new ArrayList<Ball>()));
				b.togglePreview(false);
			}
		}

		if (prev && ballsOfSteel.get(0).getOnlyNext() == null)
		{
			ballz.getChildren().clear();

			for (Ball b : ballsOfSteel)
			{
				b.togglePreview(true);
				b.reset(true, bounds);
				ballz.getChildren().addAll(b.getNext(new ArrayList<Ball>()));
			}

			ballz.getChildren().addAll(ballsOfSteel);

		}

	}

	private void adjustPevNumber(int newNumber)
	{
		togglePreview(false);
		for (Ball b : ballsOfSteel)
		{
			b.setPevNumber(newNumber);
		}
		togglePreview(true);
	}

	private void enterOptions()
	{
		optionsMode = true;
		paused = true;
	}

	private void exitOptions()
	{
		optionsMode = false;
	}

	private void createStats()
	{
		statsBg = new Rectangle();
		statsBg.setWidth(bounds.getWidth());
		statsBg.setHeight(64);
		statsBg.setFill(Color.LIGHTBLUE.brighter());
		statsBg.setStroke(Color.LIGHTBLUE.darker());
		statsBg.setStrokeWidth(padding);
		statsBg.setStrokeType(StrokeType.OUTSIDE);
		statsBg.setTranslateY(bounds.getHeight() + 16);

		stats = new GridPane();
		stats.setTranslateY(statsBg.getTranslateY());
		stats.setMinHeight(statsBg.getHeight());
		stats.setMaxHeight(statsBg.getHeight());
		stats.setMinWidth(statsBg.getWidth());
		stats.setMaxWidth(statsBg.getWidth());
		stats.setPadding(new Insets(padding));
		stats.setHgap(16);

		Font f = Font.font("Courier New", FontWeight.BLACK, 20);

		scoreA = new Label(playerA + ":"
				+ dots.substring(0,
						labelLength - (numberLength + playerA.length() + 1))
				+ String.format("%0" + numberLength + "d", winsA));
		scoreA.setFont(f);
		scoreA.setTextFill(Color.MEDIUMSLATEBLUE.darker());

		scoreB = new Label(playerB + ":"
				+ dots.substring(0,
						labelLength - (numberLength + playerB.length() + 1))
				+ String.format("%0" + numberLength + "d", winsB));
		scoreB.setFont(f);
		scoreB.setTextFill(Color.MEDIUMSLATEBLUE.darker());

		optionsButton = new Button("Options");
		optionsButton.setFont(f);
		optionsButton.setOnAction(e ->
		{
			enterOptions();
		});
		stats.add(scoreA, 0, 0);
		stats.add(scoreB, 0, 1);
		stats.add(optionsButton, 1, 0);

		GridPane.setConstraints(scoreA, 0, 0, 1, 1, HPos.LEFT, VPos.CENTER);
		GridPane.setConstraints(scoreB, 0, 1, 1, 1, HPos.LEFT, VPos.CENTER);
		GridPane.setConstraints(optionsButton, 1, 0, 1, 2, HPos.RIGHT,
				VPos.CENTER);
		GridPane.setHgrow(scoreA, Priority.ALWAYS);
		GridPane.setHgrow(scoreB, Priority.ALWAYS);
		GridPane.setHgrow(optionsButton, Priority.NEVER);
	}

	private void createPausing()
	{
		Font f = Font.font("Courier New", FontWeight.BLACK, 80);
		pausingBg = new Rectangle();
		pausingBg.setFill(Color.DARKSLATEGRAY);
		pausingBg.setOpacity(0.7);
		pausingBg.setStroke(Color.DARKSLATEGRAY);
		pausingBg.setStrokeWidth(32);
		pausingBg.setStrokeLineJoin(StrokeLineJoin.ROUND);

		pausing = new Label();
		pausing.setFont(f);
		pausing.setText("PAUSED");
		pausing.setTextFill(Color.WHITESMOKE.darker());
		pausing.setVisible(false);
	}

	private void createOptions()
	{
		darken = new Rectangle();
		darken.setWidth(bounds.getWidth());
		darken.setHeight(bounds.getHeight() + 16 + statsBg.getHeight());
		darken.setStroke(Color.BLACK);
		darken.setStrokeWidth(padding * 2);
		darken.setOpacity(0.5);

		optionsBg = new Rectangle();
		optionsBg.setWidth(bounds.getWidth() * .8);
		optionsBg.setHeight((bounds.getHeight() + 16 + stats.getHeight()) * .8);
		optionsBg.setTranslateX((bounds.getWidth() - optionsBg.getWidth()) / 2);
		optionsBg.setTranslateY(
				(bounds.getHeight() - optionsBg.getHeight()) / 2);
		optionsBg.setFill(Color.LIGHTBLUE.brighter());
		optionsBg.setStroke(Color.LIGHTBLUE.darker());
		optionsBg.setStrokeWidth(padding);
		optionsBg.setStrokeType(StrokeType.INSIDE);

		optionsGrid = new GridPane();
		optionsGrid.setMinWidth(optionsBg.getWidth() - padding * 2);
		optionsGrid.setMaxWidth(optionsBg.getWidth() - padding * 2);
		optionsGrid.setMinHeight(optionsBg.getHeight() - padding * 2);
		optionsGrid.setMaxHeight(optionsBg.getHeight() - padding * 2);
		optionsGrid.setTranslateX(optionsBg.getTranslateX() + padding);
		optionsGrid.setTranslateY(optionsBg.getTranslateY() + padding);

		optionsGrid.setPadding(new Insets(padding));
		optionsGrid.setHgap(8);
		optionsGrid.setVgap(8);

		exitOptionsButton = createButton("x", Color.rgb(240, 0, 0),
				Color.rgb(255, 30, 30));
		exitOptionsButton.setOnAction(e ->
		{
			exitOptions();
		});

		optionsGrid.add(exitOptionsButton, 4, 0);

		Font f = Font.font("Courier New", FontWeight.BLACK, 25);
		Color c = Color.DARKCYAN.darker();

		optionsTitle = new Label();
		optionsTitle.setFont(f);
		optionsTitle.setText("Options");
		optionsTitle.setTextFill(c);

		optionsGrid.add(optionsTitle, 0, 0);

		f = Font.font("Courier New", FontWeight.BLACK, 20);

		editPlayerA = new TextField();
		editPlayerA.setPromptText("Player A");
		editPlayerA.setPrefSize(150, 30);
		editPlayerA.setFont(f);
		editPlayerA.textProperty().addListener(new ChangeListener<String>()
		{
			@Override
			public void changed(ObservableValue<? extends String> observable,
					String oldValue, String newValue)
			{
				if (newValue.length() > nameLength)
				{
					editPlayerA.setText(newValue.substring(0, nameLength));
					remainingCA.setText(0 + "");
					return;
				}
				remainingCA.setText(nameLength - newValue.length() + "");
			}
		});

		optionsGrid.add(editPlayerA, 0, 1);

		editPlayerB = new TextField();
		editPlayerB.setPromptText("Player B");
		editPlayerB.setPrefSize(150, 30);
		editPlayerB.setFont(f);
		editPlayerB.textProperty().addListener(new ChangeListener<String>()
		{
			@Override
			public void changed(ObservableValue<? extends String> observable,
					String oldValue, String newValue)
			{
				if (newValue.length() > nameLength)
				{
					editPlayerB.setText(newValue.substring(0, nameLength));
					remainingCB.setText(0 + "");
					return;
				}
				remainingCB.setText(nameLength - newValue.length() + "");
			}
		});

		optionsGrid.add(editPlayerB, 0, 2);

		Tooltip remains = new Tooltip();
		Label remainsL = new Label("Remaining characters");
		remainsL.setPrefHeight(20);
		remainsL.setFont(f);
		remainsL.setTextFill(Color.WHITESMOKE);
		remains.setGraphic(remainsL);

		remainingCA = new Label();
		remainingCA.setFont(f);
		remainingCA.setTextFill(c);
		remainingCA.setTooltip(remains);

		optionsGrid.add(remainingCA, 2, 1);

		remainingCA.setText(nameLength + "");

		remainingCB = new Label();
		remainingCB.setFont(f);
		remainingCB.setTextFill(c);
		remainingCB.setTooltip(remains);

		optionsGrid.add(remainingCB, 2, 2);

		remainingCB.setText(nameLength + "");

		setPlayerA = createButton("y", Color.rgb(0, 130, 0),
				Color.rgb(0, 180, 0));
		setPlayerA.setOnAction(e ->
		{
			playerA = editPlayerA.getText();
			scoreA.setText(playerA + ":"
					+ dots.substring(0,
							labelLength - (numberLength + playerA.length() + 1))
					+ String.format("%0" + numberLength + "d", winsA));
		});

		optionsGrid.add(setPlayerA, 3, 1);

		setPlayerB = createButton("y", Color.rgb(0, 130, 0),
				Color.rgb(0, 180, 0));
		setPlayerB.setOnAction(e ->
		{
			playerB = editPlayerB.getText();
			scoreB.setText(playerB + ":"
					+ dots.substring(0,
							labelLength - (numberLength + playerB.length() + 1))
					+ String.format("%0" + numberLength + "d", winsB));
		});

		optionsGrid.add(setPlayerB, 3, 2);

		editBallCount = new TextField();
		editBallCount.setPromptText("Number of balls");
		editBallCount.setPrefSize(150, 30);
		editBallCount.setFont(f);
		editBallCount.textProperty().addListener(new ChangeListener<String>()
		{
			@Override
			public void changed(ObservableValue<? extends String> observable,
					String oldValue, String newValue)
			{
				if (!newValue.matches("\\d*"))
				{
					editBallCount.setText(newValue.replaceAll("[^\\d]", ""));
				}
				if (!editBallCount.getText().equals(""))
				{
					if (Integer.parseInt(editBallCount.getText()) > 5000)
					{
						editBallCount.setText("5000");
					} else if (Integer.parseInt(editBallCount.getText()) < 1)
					{
						editBallCount.setText("1");
					}
				}
			}
		});

		optionsGrid.add(editBallCount, 0, 3);

		showBallNumber = new Label(ballNumber + "");
		showBallNumber.setFont(f);
		showBallNumber.setTextFill(c);

		Tooltip showBalls = new Tooltip();
		Label showBallsL = new Label("Number of balls");
		showBallsL.setPrefHeight(20);
		showBallsL.setFont(f);
		showBallsL.setTextFill(Color.WHITESMOKE);
		showBalls.setGraphic(showBallsL);

		showBallNumber.setTooltip(showBalls);

		optionsGrid.add(showBallNumber, 2, 3);

		setBallCount = createButton("y", Color.rgb(0, 130, 0),
				Color.rgb(0, 180, 0));

		setBallCount.setOnAction(e ->
		{
			if (!editBallCount.getText().equals(""))
			{
				adjustBalls(Integer.parseInt(editBallCount.getText()));
			} else
			{
				adjustBalls(1);
			}
			showBallNumber.setText(ballNumber + "");
			for (int i = 0; i < ballsOfSteel.size(); i++)
			{
				ballsOfSteel.get(i).reset(false, bounds);
			}
		});

		optionsGrid.add(setBallCount, 3, 3);

		editBallSpeed = new TextField();
		editBallSpeed.setPromptText("Speed of balls");
		editBallSpeed.setPrefSize(150, 30);
		editBallSpeed.setFont(f);
		editBallSpeed.textProperty().addListener(new ChangeListener<String>()
		{
			@Override
			public void changed(ObservableValue<? extends String> observable,
					String oldValue, String newValue)
			{
				if (!newValue.matches("\\d*"))
				{
					editBallSpeed.setText(newValue.replaceAll("[^\\d]", ""));
				}
				if (!editBallSpeed.getText().equals(""))
				{
					if (Double.parseDouble(editBallSpeed.getText()) > 50)
					{
						editBallSpeed.setText("50");
					} else if (Double.parseDouble(editBallSpeed.getText()) < 1)
					{
						editBallSpeed.setText("1");
					}
				}
			}
		});

		optionsGrid.add(editBallSpeed, 0, 4);

		showBallSpeed = new Label((int) ballSpeed + "");
		showBallSpeed.setFont(f);
		showBallSpeed.setTextFill(c);

		Tooltip showSpeed = new Tooltip();
		Label showSpeedL = new Label("Speed of balls");
		showSpeedL.setPrefHeight(20);
		showSpeedL.setFont(f);
		showSpeedL.setTextFill(Color.WHITESMOKE);
		showSpeed.setGraphic(showSpeedL);

		showBallSpeed.setTooltip(showSpeed);

		optionsGrid.add(showBallSpeed, 2, 4);

		setBallSpeed = createButton("y", Color.rgb(0, 130, 0),
				Color.rgb(0, 180, 0));

		setBallSpeed.setOnAction(e ->
		{
			if (!editBallSpeed.getText().equals(""))
			{
				ballSpeed = Double.parseDouble(editBallSpeed.getText());
			} else
			{
				ballSpeed = 0;
			}
			showBallSpeed.setText((int) ballSpeed + "");
			for (int i = 0; i < ballsOfSteel.size(); i++)
			{
				ballsOfSteel.get(i).setSpeed(ballSpeed);
			}
		});

		optionsGrid.add(setBallSpeed, 3, 4);

		String[] methods = new String[]
		{"Follow closest ball", "Predict closest ball ",
				"Predict closest ball 2", "predict next ball", "aim", "aim 2"};

		f = Font.font("Courier New", FontWeight.BLACK, 20);
		ObservableList list = FXCollections.observableArrayList(methods);
		list.add(1, new Separator());
		list.add(5, new Separator());

		cAI1 = new ChoiceBox();
		cAI1.setItems(list);

		cAI1.getSelectionModel().selectedIndexProperty()
				.addListener(new ChangeListener<Number>()
				{
					@Override
					public void changed(
							ObservableValue<? extends Number> observable,
							Number oldValue, Number newValue)
					{
						ai1.setMethod(newValue.intValue());
					}
				});
		cAI1.setPrefSize(200, 30);

		Tooltip cAI1t = new Tooltip();
		Label cAI1tL = new Label("Set the method of AI 1");
		cAI1tL.setPrefHeight(20);
		cAI1tL.setFont(f);
		cAI1tL.setTextFill(Color.WHITESMOKE);
		cAI1t.setGraphic(cAI1tL);

		cAI1.setTooltip(cAI1t);

		optionsGrid.add(cAI1, 0, 10);

		cAI2 = new ChoiceBox<String>();
		cAI2.setItems(list);

		cAI2.getSelectionModel().selectedIndexProperty()
				.addListener(new ChangeListener<Number>()
				{
					@Override
					public void changed(
							ObservableValue<? extends Number> observable,
							Number oldValue, Number newValue)
					{
						ai2.setMethod(newValue.intValue());
					}
				});
		cAI2.setPrefSize(200, 30);

		Tooltip cAI2t = new Tooltip();
		Label cAI2tL = new Label("Set the method of AI 2");
		cAI2tL.setPrefHeight(20);
		cAI2tL.setFont(f);
		cAI2tL.setTextFill(Color.WHITESMOKE);
		cAI2t.setGraphic(cAI2tL);

		cAI2.setTooltip(cAI2t);

		optionsGrid.add(cAI2, 0, 11);

		tAI1 = new Button("Disable AI 1");
		tAI1.setOnAction(e ->
		{
			if (ai1.isRunning())
			{
				ai1.stopTimer();
				tAI1.setText("Enable AI 1");
			} else
			{
				ai1.startTimer();
				tAI1.setText("Disable AI 1");
			}
		});
		tAI1.setPrefSize(100, 30);

		optionsGrid.add(tAI1, 1, 10);

		tAI2 = new Button("Disable AI 2");
		tAI2.setOnAction(e ->
		{
			if (ai2.isRunning())
			{
				ai2.stopTimer();
				tAI2.setText("Enable AI 2");
			} else
			{
				ai2.startTimer();
				tAI2.setText("Disable AI 2");
			}
		});
		tAI2.setPrefSize(100, 30);

		optionsGrid.add(tAI2, 1, 11);

		tPrev = new Button("Disable Preview");
		tPrev.setOnAction(e ->
		{
			if (tPrev.getText().equals("Disable Preview"))
			{
				togglePreview(false);
				tPrev.setText("Enable Preview");
			} else
			{
				togglePreview(true);
				tPrev.setText("Disable Preview");
			}
		});
		tPrev.setPrefSize(100, 30);

		optionsGrid.add(tPrev, 1, 12);

		pevNumber = new Slider();
		pevNumber.setValue(8);
		pevNumber.valueProperty().addListener(e ->
		{
			adjustPevNumber((int) pevNumber.getValue());
		});
		pevNumber.setMajorTickUnit(5);
		pevNumber.setMinorTickCount(4);
		pevNumber.setBlockIncrement(1);
		pevNumber.setShowTickLabels(true);
		pevNumber.setShowTickMarks(true);
		pevNumber.setSnapToTicks(true);
		pevNumber.setMin(0);
		pevNumber.setMax(25);
		pevNumber.setPrefSize(200, 30);

		optionsGrid.add(pevNumber, 0, 12);

		GridPane.setConstraints(exitOptionsButton, 4, 0, 1, 1, HPos.RIGHT,
				VPos.CENTER);
		GridPane.setConstraints(optionsTitle, 0, 0, 1, 1, HPos.LEFT,
				VPos.CENTER);
		GridPane.setConstraints(editPlayerA, 0, 1, 2, 1, HPos.LEFT,
				VPos.CENTER);
		GridPane.setConstraints(editPlayerB, 0, 2, 2, 1, HPos.LEFT,
				VPos.CENTER);
		GridPane.setConstraints(editBallCount, 0, 3, 2, 1, HPos.LEFT,
				VPos.CENTER);
		GridPane.setConstraints(editBallSpeed, 0, 4, 2, 1, HPos.LEFT,
				VPos.CENTER);
		GridPane.setHgrow(optionsTitle, Priority.ALWAYS);

		options.getChildren().addAll(darken, optionsBg, optionsGrid);
		options.setVisible(false);
	}

	private Button createButton(String name, Color r1, Color r2)
	{
		Button b = new Button();
		b.setBackground(null);

		Rectangle bg = new Rectangle();
		bg.setWidth(32);
		bg.setHeight(24);
		bg.setTranslateX(-16);
		bg.setTranslateY(-12);
		bg.setStrokeLineJoin(StrokeLineJoin.ROUND);
		bg.setStrokeWidth(8);
		bg.setOpacity(0.7);

		Image xO = new Image(
				getClass().getResourceAsStream("../img/" + name + ".png"));
		ImageView xOnly = new ImageView(xO);
		xOnly.setFitWidth(24);
		xOnly.setFitHeight(24);
		xOnly.setTranslateX(-12);
		xOnly.setTranslateY(-12);

		Light.Distant light = new Light.Distant();
		light.setAzimuth(225);
		light.setColor(Color.NAVAJOWHITE);

		Lighting lighting = new Lighting();
		lighting.setLight(light);
		lighting.setSurfaceScale(2);

		bg.setEffect(lighting);

		bg.setFill(r1);
		bg.setStroke(r1);

		b.setOnMouseEntered(e ->
		{
			bg.setFill(r2);
			bg.setStroke(r2);
		});
		b.setOnMouseExited(e ->
		{
			bg.setFill(r1);
			bg.setStroke(r1);
		});
		b.setOnMousePressed(e ->
		{
			light.setAzimuth(45);
		});
		b.setOnMouseReleased(e ->
		{
			light.setAzimuth(225);
		});

		Group xSymbol = new Group();
		xSymbol.getChildren().addAll(bg, xOnly);

		b.setGraphic(xSymbol);
		b.setPadding(new Insets(0));
		return b;
	}

	private void createAI()
	{
		ai1 = new AI("A", "D", "Q", "E",
				bot.getTranslateY() - ballsOfSteel.get(0).getRadiusY(),
				top.getTranslateY() + top.getHeight()
						+ ballsOfSteel.get(0).getRadiusY(),
				p1.getTranslateX() + p1.getWidth(), p2.getTranslateX(),
				paddleHeight, 0);
		ai2 = new AI("J", "L", "U", "O",
				bot.getTranslateY() - ballsOfSteel.get(0).getRadiusY(),
				top.getTranslateY() + top.getHeight()
						+ ballsOfSteel.get(0).getRadiusY(),
				p1.getTranslateX() + p1.getWidth(), p2.getTranslateX(),
				paddleHeight, 0);

		a1 = new Ellipse(p2.getTranslateX(), ai1.getAim(), 4, 4);
		a1.setFill(Color.RED);
		a2 = new Ellipse(p1.getTranslateX() + p1.getWidth(), ai2.getAim(), 4,
				4);
		a2.setFill(Color.RED);

		playground.getChildren().addAll(a1, a2);

	}

	private void createKeyHandling()
	{
		keyPressing = new ArrayList<String>();

		time = new Timeline();
		time.setCycleCount(Timeline.INDEFINITE);
		time.getKeyFrames().add(new KeyFrame(Duration.millis(5), e ->
		{
			if (!paused)
			{
				moveAccordingToKeyAction();
				// ok
				for (int i = 0; i < ballsOfSteel.size(); i++)
				{
					int a = ballsOfSteel.get(i).moveBall(top, bot, p1, p2,
							bounds);
					if (a == 1)
					{
						winsA++;
						ballsOfSteel.get(i).reset(false, bounds);
					}
					if (a == -1)
					{
						winsB++;
						ballsOfSteel.get(i).reset(false, bounds);
					}
				}
			}
			scoreA.setText(playerA + ":"
					+ dots.substring(0,
							labelLength - (numberLength + playerA.length() + 1))
					+ String.format("%0" + numberLength + "d", winsA));
			scoreB.setText(playerB + ":"
					+ dots.substring(0,
							labelLength - (numberLength + playerB.length() + 1))
					+ String.format("%0" + numberLength + "d", winsB));
			updatePausing();
			updateOptions();
			updateAIAction();
		}));
	}

	private void updateAIAction()
	{
		ai1.updateValues(
				new Point2D(p1.getTranslateX() + p1.getWidth() + 8,
						p1.getTranslateY() + p1.getHeight() / 2),
				new Point2D(p2.getTranslateX() - 8,
						p2.getTranslateY() + p2.getHeight() / 2),
				ballsOfSteel);
		ai2.updateValues(
				new Point2D(p2.getTranslateX() - 8,
						p2.getTranslateY() + p2.getHeight() / 2),
				new Point2D(p1.getTranslateX() + p1.getWidth() + 8,
						p1.getTranslateY() + p1.getHeight() / 2),
				ballsOfSteel);

		// System.out.print("[ ");
		// for (String s : ai.getPressedKeys())
		// {
		// System.out.print(s + "; ");
		// }
		// System.out.println("]");

		a1.setTranslateY(
				ai1.getAim() + top.getTranslateY() + top.getHeight() + 8);
		a2.setTranslateY(
				ai2.getAim() + top.getTranslateY() + top.getHeight() + 8);
	}

	private void updatePausing()
	{
		if (paused)
		{
			pausing.setTranslateX((bounds.getWidth() - pausing.getWidth()) / 2);
			pausing.setTranslateY(
					(bounds.getHeight() - pausing.getHeight()) / 2);
			pausingBg.setWidth(pausing.getWidth() + 16);
			pausingBg.setHeight(pausing.getHeight() - 32);
			pausingBg.setTranslateX(
					(bounds.getWidth() - pausingBg.getWidth()) / 2);
			pausingBg.setTranslateY(
					(bounds.getHeight() - pausingBg.getHeight()) / 2);
			pausingBg.setVisible(true);
			pausing.setVisible(true);
		} else
		{
			pausingBg.setVisible(false);
			pausing.setVisible(false);
		}
	}

	private void updateOptions()
	{
		if (optionsMode)
		{
			options.setVisible(true);
		} else
		{
			options.setVisible(false);
		}
	}

	private void moveAccordingToKeyAction()
	{
		// Check for keys
		// - Player1:

		if (keyPressing.contains("A") || ai1.getPressedKeys().contains("A"))
		{
			sped1 -= dsped;
			if (sped1 < -speeeeeed)
			{
				sped1 = -speeeeeed;
			}
		} else if (keyPressing.contains("D")
				|| ai1.getPressedKeys().contains("D"))
		{
			sped1 += dsped;
			if (sped1 > speeeeeed)
			{
				sped1 = speeeeeed;
			}
		} else if (keyPressing.contains("Q")
				|| ai1.getPressedKeys().contains("Q"))
		{
			sped1 -= dsped / 16;
			if (sped1 < -speeeeeed / 4)
			{
				sped1 = -speeeeeed / 4;
			}
		} else if (keyPressing.contains("E")
				|| ai1.getPressedKeys().contains("E"))
		{
			sped1 += dsped / 16;
			if (sped1 > speeeeeed / 4)
			{
				sped1 = speeeeeed / 4;
			}
		} else
		{
			sped1 = 0;
		}

		// - Player2:

		if (keyPressing.contains("J") || ai2.getPressedKeys().contains("J"))
		{
			sped2 -= dsped;
			if (sped2 < -speeeeeed)
			{
				sped2 = -speeeeeed;
			}
		} else if (keyPressing.contains("L")
				|| ai2.getPressedKeys().contains("L"))
		{
			sped2 += dsped;
			if (sped2 > speeeeeed)
			{
				sped2 = speeeeeed;
			}
		} else if (keyPressing.contains("U")
				|| ai2.getPressedKeys().contains("U"))
		{
			sped2 -= dsped / 16;
			if (sped2 < -speeeeeed / 4)
			{
				sped2 = -speeeeeed / 4;
			}
		} else if (keyPressing.contains("O")
				|| ai2.getPressedKeys().contains("O"))
		{
			sped2 += dsped / 16;
			if (sped2 > speeeeeed / 4)
			{
				sped2 = speeeeeed / 4;
			}
		} else
		{
			sped2 = 0;
		}

		// Move paddles
		// - Player1:

		if (p1.getTranslateY() + sped1 < top.getTranslateY() + top.getHeight())
		{
			p1.setTranslateY(top.getTranslateY() + top.getHeight());
			if (sped1 < 0)
			{
				sped1 = 0;
			}
		} else if (p1.getTranslateY() + sped1 > bot.getTranslateY()
				- p1.getHeight())
		{
			p1.setTranslateY(bot.getTranslateY() - p1.getHeight());
			if (sped1 > 0)
			{
				sped1 = 0;
			}
		} else
		{
			p1.setTranslateY(p1.getTranslateY() + sped1);
		}

		// - Player2:

		if (p2.getTranslateY() + sped2 < top.getTranslateY() + top.getHeight())
		{
			p2.setTranslateY(top.getTranslateY() + top.getHeight());
			if (sped2 < 0)
			{
				sped2 = 0;
			}
		} else if (p2.getTranslateY() + sped2 > bot.getTranslateY()
				- p2.getHeight())
		{
			p2.setTranslateY(bot.getTranslateY() - p2.getHeight());
			if (sped2 > 0)
			{
				sped2 = 0;
			}
		} else
		{
			p2.setTranslateY(p2.getTranslateY() + sped2);
		}
	}

	private void addKeyPressing(String code)
	{

		if (!keyPressing.contains(code))
		{
			keyPressing.add(code);
		}
	}

	private void adjustZoom(double height, double width)
	{
		double scnRelation = height / width;
		double boundsRelation = (bounds.getHeight() + 16 + statsBg.getHeight())
				/ bounds.getWidth();

		if (scnRelation >= boundsRelation)
		{
			zoom = width / (bounds.getWidth() + padding * 2);
		} else
		{
			zoom = height / (bounds.getHeight() + padding * 2 + 16
					+ statsBg.getHeight());
		}
		playground.setScaleX(zoom);
		playground.setScaleY(zoom);
	}

	@Override
	public void start(Stage stg)
	{
		stck = new StackPane();
		stck.setBackground(null);
		stck.getChildren().addAll(playground);
		scn = new Scene(stck, 666, 666, true);
		scn.setFill(Color.rgb(5, 5, 50));

		stg.setScene(scn);
		stg.setTitle("Pong");
		stg.getIcons().add(
				new Image(getClass().getResourceAsStream("../img/icon.png")));

		scn.setOnKeyPressed(e ->
		{
			if (e.getCode() == KeyCode.P)
			{
				if (!optionsMode)
					paused = !paused;
			} else if (e.getCode() == KeyCode.ENTER)
			{
				for (int i = 0; i < ballsOfSteel.size(); i++)
				{
					ballsOfSteel.get(i).setMoving(true);
				}
			} else if (e.getCode() == KeyCode.BACK_SPACE)
			{
				for (int i = 0; i < ballsOfSteel.size(); i++)
				{
					ballsOfSteel.get(i).reset(false, bounds);
				}
			} else if (e.getCode() == KeyCode.R)
			{
				winsA = 0;
				winsB = 0;
			} else
			{
				addKeyPressing(e.getCode() + "");
			}
		});

		scn.setOnKeyReleased(e ->
		{
			if (keyPressing.contains(e.getCode() + ""))
			{
				keyPressing.remove(e.getCode() + "");
			}
		});

		scn.heightProperty().addListener(new ChangeListener<Number>()
		{

			@Override
			public void changed(ObservableValue<? extends Number> observable,
					Number oldValue, Number newValue)
			{
				adjustZoom(newValue.doubleValue(), scn.getWidth());
			}
		});

		scn.widthProperty().addListener(new ChangeListener<Number>()
		{

			@Override
			public void changed(ObservableValue<? extends Number> observable,
					Number oldValue, Number newValue)
			{
				adjustZoom(scn.getHeight(), newValue.doubleValue());
			}
		});

		cAI1.getSelectionModel().select(0);
		cAI2.getSelectionModel().select(0);

		stg.show();
		adjustZoom(scn.getHeight(), scn.getWidth());
		time.play();
		ai1.startTimer();
		ai2.startTimer();
	}

	public static void main(String[] args)
	{
		launch(args);
	}
}