package com.bemsis.platformer.classes.engine;

import java.util.ArrayList;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class Map
{
	private ArrayList<ObjectV2> objects;

	public Map()
	{
		objects = new ArrayList<>();
		createObjects();
	}

	private void createObjects()
	{
		objects.add(new ObjectV2(0, 900, 2000, 50));
		objects.add(new ObjectV2(100, 600, 50, 30));
		objects.add(new ObjectV2(500, 600, 100, 30));
		objects.add(new ObjectV2(900, 300, 30, 350));
//		objects.add(new ObjectV2(-10, 30, 2000, 20));
//		objects.add(new ObjectV2(0, 30, 20, 870));
//		objects.add(new ObjectV2(1800, 30, 20, 870));
	}

	public double[] isIntersecting(ObjectV2 player, double xSpeed, double ySpeed)
	{
		double[] i = new double[2];
		i[0] = -1;

		ArrayList<Integer> intersections = new ArrayList<>();
		for (ObjectV2 o : objects)
		{
			intersections.add(o.isIntersecting(player, xSpeed, ySpeed));
		}
		
		if (intersections.contains(-2))
		{
			i[0] = -2;
			return i;
		}

		if (intersections.contains(0))
		{
			i[0] = 0;
			return i;
		}

		if (intersections.contains(2))
		{
			i[0] = 2;
			i[1] = objects.get(intersections.indexOf(2)).getY();
			return i;
		}

		if (intersections.contains(1))
		{
			i[0] = 1;
			i[1] = (objects.get(intersections.indexOf(1)).getY() + objects.get(intersections.indexOf(1)).getHeight());
			return i;
		}

		return i;
	}

	public ArrayList<Rectangle> getMapToDraw()
	{
		ArrayList<Rectangle> a = new ArrayList<>();
		for (ObjectV2 o : objects)
		{
			Rectangle r = new Rectangle(o.getX(), o.getY(), o.getWidth(), o.getHeight());
			r.setFill(Color.RED);
			a.add(r);
		}
		return a;
	}
}
