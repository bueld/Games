package com.bemsis.platformer.classes.engine;

import java.util.ArrayList;

import javafx.geometry.Point2D;

public class Player
{
	private Point2D coords;
	private boolean inAir = true;
	private boolean jumpWanted = false;
	private boolean forceDown = false;

	private double xSpeed = 0;
	private double ySpeed = 0;

	private int currentIntersection = -1;

	public Player(String name, Point2D startCoords)
	{
		coords = startCoords;
	}

	public void move(Map map, ArrayList<String> pressedKeys)
	{
//		System.out.println("moved");
		
		ArrayList<String> strs = cleanKeys(pressedKeys);

		int direction = 0;
		double sprint = 1;

		if (strs.contains("SHIFT"))
		{
			sprint = 1.8;
		}
		jumpWanted = false;
		forceDown = false;

		for (int i = 0; i < strs.size(); i++)
		{
			switch (strs.get(i))
			{
			case "A":
				direction = -1;
				break;
			case "D":
				direction = 1;
				break;
			case "W":
				jumpWanted = true;
				break;
			case "S":
				forceDown = true;
				break;
			case "SPACE":
				jumpWanted = true;
				break;
			}
		}

		moveAndJump(direction, sprint);

		constrainOnMap(map);
	}

	private void moveAndJump(int direction, double sprint)
	{
		double x = coords.getX();
		double y = coords.getY();

		xSpeed += direction * EngineReferences.playerHorizontalAcceleration * sprint;

		if (direction == 0)
		{
			xSpeed = 0;
		}

		if (Math.abs(xSpeed) > EngineReferences.playerMaxHorizontalSpeed * sprint)
		{
			xSpeed /= Math.abs(xSpeed) / EngineReferences.playerMaxHorizontalSpeed / sprint;
		}

		if (!inAir)
		{
			xSpeed *= EngineReferences.playerGroundMaxSpeedFactor;
		}

		if (jumpWanted && !inAir)
		{
			ySpeed = EngineReferences.playerVerticalStartSpeed;
			inAir = true;
		} else if (inAir && !forceDown)
		{
			ySpeed += EngineReferences.playerVerticalAcceleration;
		} else if (inAir && forceDown)
		{
			ySpeed += EngineReferences.playerVerticalAcceleration * 2;
		}

		x += xSpeed;
		y += ySpeed;

		coords = new Point2D(x, y);
	}

	private void constrainOnMap(Map map)
	{
		double[] i = map.isIntersecting(
				new ObjectV2(coords.getX(), coords.getY(), EngineReferences.playerWidth, EngineReferences.playerWidth),
				xSpeed, ySpeed);
		switch ((int) i[0])
		{
//		 0: Hitting left or right
//		 1: Hitting with upper edge
//		 2: Standing on ground
//	    -1: Nothing (ergo inAir)
//		-2: STOP!!!!!!!!! (Dev only)
		case 0:
			xSpeed = 0;
			break;
		case 1:
			ySpeed = 0;
			coords = new Point2D(coords.getX(), i[1] + 0.5);
			break;
		case 2:
			ySpeed = 0;
			coords = new Point2D(coords.getX(), i[1] - EngineReferences.playerHeight + 0.3);
			inAir = false;
			break;
		case -1:
			inAir = true;
			break;
		case -2:
			inAir = false;
			ySpeed = 0;
			xSpeed = 0;
			System.out.println("stalled");
			break;
		}

		currentIntersection = (int) i[0];
	}

	public int getCurentAnimation()
	{
		if (inAir)
		{
			return 1;
		} else
		{
			return 0;
		}
	}

	private ArrayList<String> cleanKeys(ArrayList<String> keys)
	{
		ArrayList<String> a = new ArrayList<String>();

		if ((keys.contains("W") && !keys.contains("S")) || (keys.contains("UP") && !keys.contains("DOWN"))
				|| (keys.contains("SPACE") && !keys.contains("DOWN"))
				|| (keys.contains("SPACE") && !keys.contains("S")))
		{
			a.add("W");
		}
		if ((!keys.contains("W") && keys.contains("S")) || (!keys.contains("UP") && keys.contains("DOWN"))
				|| (!keys.contains("SPACE") && keys.contains("DOWN"))
				|| (!keys.contains("SPACE") && keys.contains("S")))
		{
			a.add("S");
		}
		if ((keys.contains("A") && !keys.contains("D")) || (keys.contains("LEFT") && !keys.contains("RIGHT")))
		{
			a.add("A");
		}
		if ((!keys.contains("A") && keys.contains("D")) || (!keys.contains("LEFT") && keys.contains("RIGHT")))
		{
			a.add("D");
		}

		if (keys.contains("SHIFT"))
		{
			a.add("SHIFT");
		}
		if (keys.contains("SPACE"))
		{
			a.add("SPACE");
		}
		if (keys.contains("ENTER"))
		{
			inAir = false;
		}

		return a;
	}

	public void setCoords(Point2D coords)
	{
		this.coords = coords;
		xSpeed = 0;
		ySpeed = 0;
	}

	public Point2D getCoords()
	{
		return coords;
	}

	public int getCurrentIntersectionType()
	{
		return currentIntersection;
	}

}
