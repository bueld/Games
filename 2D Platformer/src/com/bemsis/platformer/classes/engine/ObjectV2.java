package com.bemsis.platformer.classes.engine;

import javafx.geometry.Bounds;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;

public class ObjectV2
{
	private double x, y, width, height;

	public ObjectV2(double x, double y, double width, double height)
	{
		this.setX(x);
		this.setY(y);
		this.setWidth(width);
		this.setHeight(height);
	}

	public int isIntersecting(ObjectV2 p, double xSpeed, double ySpeed)
	{
//		 0: Hitting left or right
//		 1: Hitting with upper edge
//		 2: Standing on ground
//	    -1: Nothing (ergo inAir)
//		-2: STOP!!!!!!!!! (Dev only)

		Rectangle pl = new Rectangle(p.getX() - 1, p.getY() - 1, p.getWidth() + 2, p.getHeight() + 2);
		Rectangle obj = new Rectangle(p.getX(), p.getY(), p.getWidth(), p.getHeight());

		if (obj.intersects(pl.getBoundsInParent()))
		{
			Shape s = Shape.intersect(obj, pl);
			Bounds b = s.getBoundsInParent();

			if (b.getMaxY()>= this.getY() && b.getMaxY()<= this.getY()+this.getHeight())
			{
				System.out.println("floored");
				return 2;
			}
		}

		return -1;
	}

	public double getX()
	{
		return x;
	}

	public void setX(double x)
	{
		this.x = x;
	}

	public double getY()
	{
		return y;
	}

	public void setY(double y)
	{
		this.y = y;
	}

	public double getWidth()
	{
		return width;
	}

	public void setWidth(double width)
	{
		this.width = width;
	}

	public double getHeight()
	{
		return height;
	}

	public void setHeight(double height)
	{
		this.height = height;
	}
}
