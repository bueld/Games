package com.bemsis.platformer.classes.engine;

import javafx.geometry.Point2D;

public final class EngineReferences
{
	public static final double playerMaxHorizontalSpeed = 6;
	public static final double playerVerticalAcceleration = 0.5;
	public static final double playerHorizontalAcceleration = 0.2;
	public static final double playerVerticalStartSpeed = -18;
	public static final double playerGroundMaxSpeedFactor = 3;
	
	public static final Point2D playerstartCoords = new Point2D(400,850);
	
	public static final double playerWidth = 21;
	public static final double playerHeight = 32;
}
