package com.bemsis.platformer.classes.engine;

public class Object
{
	private double x, y, width, height;

	public Object(double x, double y, double width, double height)
	{
		this.setX(x);
		this.setY(y);
		this.setWidth(width);
		this.setHeight(height);
	}

	public int isIntersecting(Object p, double xSpeed, double ySpeed)
	{

//		if (p.getY() + ySpeed < this.y + this.height && p.getY() + ySpeed > this.y
//				&& ((p.getX() + xSpeed < this.x + this.width && p.getX() + xSpeed > this.x)
//						|| (p.getX() + p.getWidth() + xSpeed < this.x + this.width
//								&& p.getX() + p.getWidth() + xSpeed > this.x)))
//		{
//			return 1;
//		}
//
//		if (p.getY() + p.getHeight() + ySpeed < this.y + this.height && p.getY() + p.getHeight() + ySpeed > this.y
//				&& ((p.getX() + xSpeed < this.x + this.width && p.getX() + xSpeed > this.x)
//						|| (p.getX() + p.getWidth() + xSpeed < this.x + this.width
//								&& p.getX() + p.getWidth() + xSpeed > this.x)))
//		{
//			return 2;
//		}
//		if (((p.getX() - 8 + xSpeed < this.x + width && p.getX() - 8 + xSpeed > this.x)
//				|| (p.getX() + xSpeed + p.getWidth() + 8 > this.x
//						&& p.getX() + 8 + xSpeed + p.getWidth() < this.x + this.width))
//				&& ((p.getY() + EngineReferences.playerHeight + ySpeed < this.y + this.height
//						&& p.getY() + EngineReferences.playerHeight + ySpeed > this.y)
//						|| (p.getY() + p.getHeight() - EngineReferences.playerHeight + ySpeed < this.y + this.height
//								&& p.getY() - EngineReferences.playerHeight + p.getHeight() + ySpeed > this.y)))
//		{
//			return 0;
//		}
//
		if ((p.getX() < this.x + this.width && p.getX() > this.x)
				|| (p.getX() + p.getWidth() < this.x + this.width && p.getX() + p.getWidth() > this.x))
		{
			if (p.getY() + p.getHeight() + ySpeed + 0.5 > this.y && p.getY() + p.getHeight() < this.y)
			{
				return 2;
			}

			if (p.getY() + p.getHeight() < this.y && p.getY() + p.getHeight() + 2 > this.y)
			{
				return -2;
			}

		}

		return -1;

	}

	public double getX()
	{
		return x;
	}

	public void setX(double x)
	{
		this.x = x;
	}

	public double getY()
	{
		return y;
	}

	public void setY(double y)
	{
		this.y = y;
	}

	public double getWidth()
	{
		return width;
	}

	public void setWidth(double width)
	{
		this.width = width;
	}

	public double getHeight()
	{
		return height;
	}

	public void setHeight(double height)
	{
		this.height = height;
	}
}
