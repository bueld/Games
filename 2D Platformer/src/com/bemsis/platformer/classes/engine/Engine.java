package com.bemsis.platformer.classes.engine;

import java.util.ArrayList;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.geometry.Point2D;
import javafx.util.Duration;

public class Engine
{
	private Timeline timer;
	private Map map;
	private Player player1;
	private ArrayList<String> pressedKeys;

	private boolean paused = false;

	public Engine(Duration d, String playerName)
	{
		createTimer(d);

		createPlayer(playerName);
		createMap();
	}

	private void tick()
	{
		refreshPlayer();
//		refreshMap();
//		refreshScore();
	}

	private void refreshPlayer()
	{
		player1.move(map, pressedKeys);

	}

	private void createPlayer(String playerName)
	{
		pressedKeys = new ArrayList<>();
		player1 = new Player(playerName, EngineReferences.playerstartCoords);

	}

	private void createMap()
	{
		map = new Map();
	}

	private void createTimer(Duration d)
	{
		timer = new Timeline();
		timer.setCycleCount(Timeline.INDEFINITE);
		timer.getKeyFrames().add(new KeyFrame(d, e ->
		{
			tick();
		}));

	}

	public void switchPause()
	{
		paused ^= true;

		if (paused)
		{
			timer.pause();
			System.out.println("paused");
		} else
		{
			timer.play();
			System.out.println("UNpaused");
		}
	}

	public void startTimer()
	{
		timer.play();
	}

	public void pauseTimer()
	{
		timer.pause();
	}

	public int getCurrentPlayerAnimation()
	{
		return player1.getCurentAnimation();
	}

	public int getCurrentIntersectionType()
	{
		return player1.getCurrentIntersectionType();
	}

	public Point2D getPlayerCoords()
	{
		return player1.getCoords();
	}

	public Map getMap()
	{
		return map;
	}

	public void rePlacePlayer(Point2D p)
	{
		player1.setCoords(p);
	}

	public void setW(boolean W)
	{
		if (W && !pressedKeys.contains("W"))
		{
			pressedKeys.add("W");
		} else if (!W && pressedKeys.contains("W"))
		{
			pressedKeys.remove("W");
		}

	}

	public void setA(boolean A)
	{
		if (A && !pressedKeys.contains("A"))
		{
			pressedKeys.add("A");
		} else if (!A && pressedKeys.contains("A"))
		{
			pressedKeys.remove("A");
		}
	}

	public void setS(boolean S)
	{
		if (S && !pressedKeys.contains("S"))
		{
			pressedKeys.add("S");
		} else if (!S && pressedKeys.contains("S"))
		{
			pressedKeys.remove("S");
		}
	}

	public void setD(boolean D)
	{
		if (D && !pressedKeys.contains("D"))
		{
			pressedKeys.add("D");
		} else if (!D && pressedKeys.contains("D"))
		{
			pressedKeys.remove("D");
		}
	}

	public void setUP(boolean UP)
	{
		if (UP && !pressedKeys.contains("UP"))
		{
			pressedKeys.add("UP");
		} else if (!UP && pressedKeys.contains("UP"))
		{
			pressedKeys.remove("UP");
		}
	}

	public void setLEFT(boolean LEFT)
	{
		if (LEFT && !pressedKeys.contains("LEFT"))
		{
			pressedKeys.add("LEFT");
		} else if (!LEFT && pressedKeys.contains("LEFT"))
		{
			pressedKeys.remove("LEFT");
		}

	}

	public void setDOWN(boolean DOWN)
	{
		if (DOWN && !pressedKeys.contains("DOWN"))
		{
			pressedKeys.add("DOWN");
		} else if (!DOWN && pressedKeys.contains("DOWN"))
		{
			pressedKeys.remove("DOWN");
		}
	}

	public void setRIGHT(boolean RIGHT)
	{
		if (RIGHT && !pressedKeys.contains("RIGHT"))
		{
			pressedKeys.add("RIGHT");
		} else if (!RIGHT && pressedKeys.contains("RIGHT"))
		{
			pressedKeys.remove("RIGHT");
		}
	}

	public void setP(boolean P)
	{
		if (P && !pressedKeys.contains("P"))
		{
			pressedKeys.add("P");
		} else if (!P && pressedKeys.contains("P"))
		{
			pressedKeys.remove("P");
		}
	}

	public void setSPACE(boolean SPACE)
	{
		if (SPACE && !pressedKeys.contains("SPACE"))
		{
			pressedKeys.add("SPACE");
		} else if (!SPACE && pressedKeys.contains("SPACE"))
		{
			pressedKeys.remove("SPACE");
		}
	}

	public void setESC(boolean ESC)
	{
		if (ESC && !pressedKeys.contains("ESC"))
		{
			pressedKeys.add("ESC");
		} else if (!ESC && pressedKeys.contains("ESC"))
		{
			pressedKeys.remove("ESC");
		}
	}

	public void setENTER(boolean ENTER)
	{
		if (ENTER && !pressedKeys.contains("ENTER"))
		{
			pressedKeys.add("ENTER");
		} else if (!ENTER && pressedKeys.contains("ENTER"))
		{
			pressedKeys.remove("ENTER");
		}
	}

	public void setSHIFT(boolean SHIFT)
	{
		if (SHIFT && !pressedKeys.contains("SHIFT"))
		{
			pressedKeys.add("SHIFT");
		} else if (!SHIFT && pressedKeys.contains("SHIFT"))
		{
			pressedKeys.remove("SHIFT");
		}
	}
}
