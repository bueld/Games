package com.bemsis.platformer.classes.UI;

import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;

public class Bar extends MenuBar
{
	public Bar(boolean settings, Node set, boolean language, Node lang)
	{
		this.setPadding(new Insets(2));

		if (settings)
			addSettings(set);

		if (language)
			addLanguage(lang);
	}

	private void addLanguage(Node lang)
	{
		Menu menu = new Menu("Language");
		menu.setGraphic(lang);

		MenuItem m1 = new MenuItem("Language 1");
		m1.setOnAction(e ->
		{

		});
		MenuItem m2 = new MenuItem("Language 2");
		m1.setOnAction(e ->
		{

		});
		MenuItem m3 = new MenuItem("Language 3");
		m1.setOnAction(e ->
		{

		});

		menu.getItems().addAll(m1, m2, m3);

		this.getMenus().add(menu);
	}

	private void addSettings(Node set)
	{
		Menu menu = new Menu("Settings");
		menu.setGraphic(set);

		Menu m1 = new Menu("Setting 1");
		Menu m2 = new Menu("Setting 2");
		Menu m3 = new Menu("Setting 3");

		menu.getItems().addAll(m1, m2, new SeparatorMenuItem(), m3);

		this.getMenus().add(menu);
	}
}
