package com.bemsis.platformer.classes.UI;

import java.util.ArrayList;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.VPos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.effect.Light;
import javafx.scene.effect.Lighting;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.Priority;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class FrameWork extends GridPane
{
	private Pane pn;
	private GridPane gPn;
	private String name;
	private Image img;
	private MenuBar mBar;

	private static final int topBarHeight = 32;

	private ArrayList<Button> utilities;

	public FrameWork(Pane pane, String title, Image icon, MenuBar menuBar, Background b, Stage stg)
	{
		this.pn = pane;
		this.setTitle(title);
		this.setIcon(icon);
		this.setMenuBar(menuBar);

		createGPn(b, stg);

		this.add(gPn, 0, 0);
		this.add(pn, 0, 1);

		this.setBackground(b);
	}

	private void createGPn(Background b, Stage stg)
	{
		gPn = new GridPane();
		gPn.setPadding(new Insets(6));

		gPn.setBackground(b);

		ImageView imgView = new ImageView(img);
		imgView.setPreserveRatio(true);
		imgView.setFitHeight(topBarHeight);
		gPn.add(imgView, 0, 0);

		Label label = new Label(name);
		label.setFont(Font.font("Arial", FontWeight.EXTRA_BOLD, topBarHeight));
		label.setTextFill(Color.WHITESMOKE);

		Light.Distant light = new Light.Distant();
		light.setColor(Color.rgb(220, 20, 140));
		light.setAzimuth(225);

		Lighting l = new Lighting(light);
		label.setPadding(new Insets(-2, 2, 2, 2));

		label.setEffect(l);
		gPn.add(label, 1, 0);

		gPn.setHgap(3);

		createUtilities(stg);

		GridPane.setConstraints(imgView, 0, 0, 1, 1, HPos.LEFT, VPos.TOP, Priority.NEVER, Priority.NEVER);
		GridPane.setConstraints(label, 1, 0, 1, 1, HPos.LEFT, VPos.TOP, Priority.ALWAYS, Priority.ALWAYS);

		gPn.add(mBar, 0, 1);

		GridPane.setConstraints(mBar, 0, 1, 5, 1);

	}

	private void createUtilities(Stage stg)
	{
		utilities = new ArrayList<>();

		Button minimise = createUtilityButton("minimise_0", createActionEvent("minimise", stg));
		Button maximise = createUtilityButton("maximise_0", createActionEvent("maximise", stg));
		Button close = createUtilityButton("close_0", createActionEvent("close", stg));
		
		minimise.setFocusTraversable(false);
		maximise.setFocusTraversable(false);
		close.setFocusTraversable(false);

		utilities.add(minimise);
		utilities.add(maximise);
		utilities.add(close);

		gPn.add(utilities.get(0), 2, 0);
		gPn.add(utilities.get(1), 3, 0);
		gPn.add(utilities.get(2), 4, 0);

		GridPane.setConstraints(minimise, 2, 0, 1, 1, HPos.RIGHT, VPos.TOP, Priority.NEVER, Priority.NEVER);
		GridPane.setConstraints(maximise, 3, 0, 1, 1, HPos.RIGHT, VPos.TOP, Priority.NEVER, Priority.NEVER);
		GridPane.setConstraints(close, 4, 0, 1, 1, HPos.RIGHT, VPos.TOP, Priority.NEVER, Priority.NEVER);
	}

	private Button createUtilityButton(String file, EventHandler<ActionEvent> handler)
	{
		ImageView idle = new ImageView(
				new Image(getClass().getResourceAsStream("../../img/UI/" + file + "/" + file + "_idle.png")));
		ImageView hov = new ImageView(
				new Image(getClass().getResourceAsStream("../../img/UI/" + file + "/" + file + "_hov.png")));
		ImageView press = new ImageView(
				new Image(getClass().getResourceAsStream("../../img/UI/" + file + "/" + file + "_press.png")));

		idle.setPreserveRatio(true);
		hov.setPreserveRatio(true);
		press.setPreserveRatio(true);
		idle.setFitHeight(topBarHeight);
		hov.setFitHeight(topBarHeight);
		press.setFitHeight(topBarHeight);

		Button b = new Button();
		b.setGraphic(idle);
		b.setPadding(new Insets(2));
		b.setBackground(null);
		b.setOnMouseEntered(e ->
		{
			b.setGraphic(hov);
		});
		b.setOnMouseExited(e ->
		{
			b.setGraphic(idle);
		});
		b.setOnMousePressed(e ->
		{
			b.setGraphic(press);
		});
		b.setOnMouseReleased(e ->
		{
			b.setGraphic(idle);
		});
		b.setOnAction(handler);

		return b;
	}

	private EventHandler<ActionEvent> createActionEvent(String type, Stage stg)
	{
		EventHandler<ActionEvent> e = null;
		switch (type)
		{
		case "close":
			e = new EventHandler<ActionEvent>()
			{
				@Override
				public void handle(ActionEvent arg0)
				{
					System.exit(0);
				}
			};
			break;
		case "minimise":
			e = new EventHandler<ActionEvent>()
			{
				@Override
				public void handle(ActionEvent arg0)
				{
					stg.setIconified(true);
				}
			};
			break;

		case "maximise":
			e = new EventHandler<ActionEvent>()
			{
				@Override
				public void handle(ActionEvent arg0)
				{
					stg.setMaximized(!stg.isMaximized());
					restoreMaximiseButton(stg.isMaximized(), stg);
				}
			};
			break;
		}
		return e;
	}

	private void restoreMaximiseButton(boolean isMaximised, Stage stg)
	{
		gPn.getChildren().remove(utilities.get(1));
		utilities.remove(1);
		Button b;

		if (isMaximised)
		{
			b = createUtilityButton("restore_0", createActionEvent("maximise", stg));
		} else
		{
			b = createUtilityButton("maximise_0", createActionEvent("maximise", stg));
		}
		utilities.add(1, b);
		gPn.add(utilities.get(1), 3, 0);
	}

	public String getTitle()
	{
		return name;
	}

	public void setTitle(String title)
	{
		this.name = title;
	}

	public Image getIcon()
	{
		return img;
	}

	public void setIcon(Image icon)
	{
		this.img = icon;
	}

	public MenuBar getMenuBar()
	{
		return mBar;
	}

	public void setMenuBar(MenuBar menuBar)
	{
		this.mBar = menuBar;
	}

	public void resizeWidth(double width)
	{
		this.setPrefWidth(width);
		gPn.setPrefWidth(width);
	}

}
