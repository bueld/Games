package com.bemsis.platformer.classes.UI;

import java.io.File;
import java.util.ArrayList;

import com.bemsis.platformer.classes.engine.Engine;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.geometry.Point2D;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.util.Duration;

public class Graphics extends Pane
{
	private Timeline timer;
	private Engine eng;

	private ArrayList<Image> player;
	private Point2D playerCoords;
	private ImageView playerImg;
	
	private Label currIntersectionType;

	public Graphics(Duration d, Engine engine)
	{
		this.setBackground(null);
		this.eng = engine;

		createPlayer();
		createMap();
		
		createLabel();

		createTimer(d);
	}
	
	private void createLabel()
	{
		currIntersectionType = new Label("-1");
		currIntersectionType.setFont(Font.font("Arial", FontWeight.EXTRA_BOLD, 32));
		currIntersectionType.setTextFill(Color.WHITESMOKE);
		currIntersectionType.setTranslateX(20);
		this.getChildren().add(currIntersectionType);
	}
	
	private void updateValues()
	{
		playerCoords = eng.getPlayerCoords();
		playerImg.setImage(player.get(eng.getCurrentPlayerAnimation()));
		
		currIntersectionType.setText(eng.getCurrentIntersectionType()+"");
	}

	private void draw()
	{
		playerImg.setTranslateX(playerCoords.getX());
		playerImg.setTranslateY(playerCoords.getY());
	}

	private void createPlayer()
	{
		loadPlayerImg();
		playerCoords = eng.getPlayerCoords();
		playerImg = new ImageView(player.get(eng.getCurrentPlayerAnimation()));
		playerImg.setPreserveRatio(true);
		playerImg.setFitHeight(32);
		
		this.getChildren().add(playerImg);
	}

	private void loadPlayerImg()
	{
		player = new ArrayList<>();

		File folder = new File("src/com/bemsis/platformer/img/game/player");

		File[] repository = folder.listFiles();

		for (File f : repository)
		{
			player.add(new Image(f.toURI().toString()));
		}
	}

	private void createMap()
	{
		this.getChildren().addAll(eng.getMap().getMapToDraw());
	}

	private void createTimer(Duration d)
	{
		timer = new Timeline();
		timer.setCycleCount(Timeline.INDEFINITE);
		timer.getKeyFrames().add(new KeyFrame(d, e ->
		{
			requestFocus();
			updateValues();
			draw();
		}));
	}

	public void startTimer()
	{
		timer.play();
	}

	public void pauseTimer()
	{
		timer.pause();
	}
}
