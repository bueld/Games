package com.bemsis.platformer.classes.UI;

import com.bemsis.platformer.classes.engine.Engine;
import com.bemsis.platformer.classes.engine.EngineReferences;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

public class Main extends Application
{
	private Stage stage;
	private Scene scn;
	private Graphics pane;
	private Engine eng;

	private FrameWork frameWork;

	public void init()
	{
		eng = new Engine(Duration.millis(25), "Bemsis");
		pane = new Graphics(Duration.millis(16), eng);
	}

	private void initFrameWork()
	{
		Image img = new Image(getClass().getResourceAsStream("../../img/UI/icon.png"));
		frameWork = new FrameWork(pane, "2D Platformer by Bemsis", img, new Bar(true, null, true, null), null, stage);

		scn = new Scene(frameWork, 600, 600, Color.rgb(40, 8, 60));
	}

	private void addKeyListeners()
	{
		scn.setOnKeyPressed(e ->
		{
			switch (e.getCode())
			{
			case W:
				eng.setW(true);
				break;
			case A:
				eng.setA(true);
				break;
			case S:
				eng.setS(true);
				break;
			case D:
				eng.setD(true);
				break;
			case P:
				eng.setP(true);
				break;
			case UP:
				eng.setUP(true);
				break;
			case LEFT:
				eng.setLEFT(true);
				break;
			case DOWN:
				eng.setDOWN(true);
				break;
			case RIGHT:
				eng.setRIGHT(true);
				break;
			case SPACE:
				eng.setSPACE(true);
				break;
			case ENTER:
				eng.setENTER(true);
				break;
			case SHIFT:
				eng.setSHIFT(true);
				break;
			case ESCAPE:
				eng.setESC(true);
				break;
			case R:
				eng.rePlacePlayer(EngineReferences.playerstartCoords);
				break;
			default:
				break;

			}
		});

		scn.setOnKeyReleased(e ->
		{
			switch (e.getCode())
			{
			case W:
				eng.setW(false);
				break;
			case A:
				eng.setA(false);
				break;
			case S:
				eng.setS(false);
				break;
			case D:
				eng.setD(false);
				break;
			case P:
				eng.setP(false);
				eng.switchPause();
				break;
			case UP:
				eng.setUP(false);
				break;
			case LEFT:
				eng.setLEFT(false);
				break;
			case DOWN:
				eng.setDOWN(false);
				break;
			case RIGHT:
				eng.setRIGHT(false);
				break;
			case SPACE:
				eng.setSPACE(false);
				break;
			case ENTER:
				eng.setENTER(false);
				break;
			case SHIFT:
				eng.setSHIFT(false);
				break;
			case ESCAPE:
				eng.setESC(false);
				break;
			default:
				break;

			}
		});
	}

	@Override
	public void start(Stage stg) throws Exception
	{
		stage = new Stage();
		stage = stg;
		initFrameWork();
		addKeyListeners();
		stage.setScene(scn);
		stage.setTitle("2D Platformer by Bemsis");

		stage.initStyle(StageStyle.TRANSPARENT);

		stage.widthProperty().addListener(e ->
		{
			frameWork.resizeWidth(stage.getWidth());
		});

		stage.show();

		eng.startTimer();
		pane.startTimer();
		
		eng.switchPause();
	}

	public static void main(String[] args)
	{
		launch(args);
	}

}
